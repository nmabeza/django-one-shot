from django.shortcuts import render
from .models import TodoList, TodoItem
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


# Create your views here.

class TodoListView(ListView):
    model = TodoList
    template_name = 'todos/todolist.html'
    context_object_name = "todos"

class TodoDetailView(DetailView):
    model = TodoList
    template_name = 'todos/tododetail.html'

class TodoCreateView(CreateView):
    model = TodoList
    template_name = 'todos/todocreate.html'
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name= 'todos/todoupdate.html'
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = 'todos/tododelete.html'
    success_url=reverse_lazy("todo_list_list")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = 'todos/todoitemcreate.html'
    fields = "__all__"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name= 'todos/todoitemupdate.html'
    fields = "__all__"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])
